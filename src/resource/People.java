package resource;

import java.util.ArrayList;
import java.util.List;

public class People {
    public static List<Person> getPeople() {
        List<Person> people = new ArrayList<>();
        people.add(new Person(20, "Kirilla"));
        people.add(new Person(80, "Vasilii"));
        people.add(new Person(25, "Maksim"));
        people.add(new Person(65, "Vlad"));
        people.add(new Person(20, "Evgena"));
        return people;
    }
}

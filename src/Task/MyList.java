package Task;

import resource.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MyList {
    public List<Person> task1RemoveMax(List<Person> personList) {
        List<Person> resultList = new ArrayList<Person>();
        for (Person obj : personList) {
            if (obj.getAge() != Collections.max(personList, new Comparator<Person>() {
                @Override
                public int compare(Person o1, Person o2) {
                    return o1.getAge().compareTo(o2.getAge());
                }
            }).getAge()) {
                resultList.add(obj);
            }
        }
        return resultList;
    }

    public List<Person> task1RemoveMin(List<Person> personList) {
        List<Person> resultList = new ArrayList<Person>();
        for (Person obj : personList) {
            if (obj.getAge() != Collections.min(personList, new Comparator<Person>() {
                @Override
                public int compare(Person o1, Person o2) {
                    return o1.getAge().compareTo(o2.getAge());
                }
            }).getAge()) {
                resultList.add(obj);
            }
        }
        return resultList;
    }

    public List<Person> task2RemoveMoreX(List<Person> personList, int x) {
        List<Person> resultList = new ArrayList<Person>();
        for (Person obj : personList) {
            if (obj.getAge() < x) {
                resultList.add(obj);
            }
        }
        return resultList;
    }

    public List<Person> task2RemoveLessX(List<Person> personList, int x) {
        List<Person> resultList = new ArrayList<Person>();
        for (Person obj : personList) {
            if (obj.getAge() > x) {
                resultList.add(obj);
            }
        }
        return resultList;
    }

    public Integer task3TotalSum(List<Person> personList) {
        Integer sum = 0;
        for (Person obj : personList) {
            sum += obj.getAge();
        }
        return sum;
    }

    public Person task4GetForIndex(List<Person> personList, int index) {
        return index >= personList.size() ? null : personList.get(index);
    }

    public List<Person> task5Get2Elements(List<Person> personList, int start, int number) {
        if (start + number > personList.size()) {
            return null;
        }
        List<Person> resultList = new ArrayList<Person>();
        for (int i = 0; i < number; i++, start++) {
            resultList.add(personList.get(start));
        }
        return resultList;
    }

    public boolean task6SymbolSearch(List<Person> personList, CharSequence symbol) {
        for (Person obj : personList) {
            if (obj.getName().indexOf(symbol.toString()) == -1) {
                return false;
            }
        }
        return true;
    }

    public List<Person> task7RenameObject(List<Person> personList) {
        List resultList = new ArrayList();
        for (Person obj : personList) {
            resultList.add(obj.toString() + "_1");
        }

        return resultList;
    }

    public List<Person> task8SortAge(List<Person> personList) {
        List<Person> resultList = new ArrayList<Person>(personList);
        resultList.sort(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getAge().compareTo(o2.getAge());
            }
        });
        return resultList;
    }

    public List<Person> task8SortName(List<Person> personList) {
        List<Person> resultList = new ArrayList<Person>(personList);
        resultList.sort(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return resultList;
    }

    public List<Person> task9TakePattern(List<Person> personList, String pattern) {
        List<Person> resultList = new ArrayList<Person>();
        for (Person obj : personList) {
            if (obj.getName().contains(pattern)) {
                resultList.add(obj);
            }
        }
        return resultList;
    }
}

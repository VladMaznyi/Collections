package Task;

import resource.People;
import resource.Person;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Runner {
    public static void main(String[] args) {
        int x = 30;
        int index = 2;
        CharSequence symbol = "a";
        String pattern = "la";


        List<Person> people = People.getPeople();
        Map<String, Person> map = new HashMap<>();
        for (Person person : people) {
            map.put(person.getName(), person);
        }

        MyMap myMap = new MyMap();

        System.out.println("Work with MAP");
        System.out.println("1)ma Map without min: " + myMap.task1RemoveMin(map));
        System.out.println("1)ma Map without max: " + myMap.task1RemoveMax(map));
        System.out.println("2)ma Remove more than " + x + ": " + myMap.task2RemoveMoreX(map, x));
        System.out.println("2)ma Remove less than " + x + ": " + myMap.task2RemoveLessX(map, x));
        System.out.println("3)ma Total age value: " + myMap.task3TotalSum(map));
        System.out.println("4)ma " + (index + 1) + " Element in order: " + myMap.task4GetForIndex(map, index));
        System.out.println("5)ma Return two elements from the second" + myMap.task5Get2Elements(map, 1, 2));
        System.out.println("6)ma Search '" + symbol + "': " + myMap.task6SymbolSearch(map, symbol));
        System.out.println("7)ma Rename objects: " + myMap.task7RenameObject(map));
        System.out.println("8)ma Sorted by age: " + myMap.task8SortAge(map));
        System.out.println("8)ma Sorted by name: " + myMap.task8SortName(map));
        System.out.println("9)ma Template for " + pattern + ": " + myMap.task9TakePattern(map, pattern));

        MyList myList = new MyList();
        Lambda lambda = new Lambda();

        System.out.println("\n\nWork with List");
        System.out.println("1)li List without min: " + myList.task1RemoveMin(people));
        System.out.println("1)la List without min: " + lambda.task1RemoveMin(people));
        System.out.println("1)li List without max: " + myList.task1RemoveMax(people));
        System.out.println("1)la List without max: " + lambda.task1RemoveMax(people));
        System.out.println("2)li Remove more than " + x + ": " + myList.task2RemoveMoreX(people, x));
        System.out.println("2)la Remove more than " + x + ": " + lambda.task2RemoveMoreX(people, x));
        System.out.println("2)li Remove less than " + x + ": " + myList.task2RemoveLessX(people, x));
        System.out.println("2)la Remove less than " + x + ": " + lambda.task2RemoveLessX(people, x));
        System.out.println("3)li Total age value: " + myList.task3TotalSum(people));
        System.out.println("3)la Total age value: " + lambda.task3TotalSum(people));
        System.out.println("4)li " + (index + 1) + " Element in order: " + myList.task4GetForIndex(people, index));
        System.out.println("4)la " + (index + 1) + " Element in order: " + lambda.task4GetForIndex(people, index));
        System.out.println("5)li Return two elements from the second" + myList.task5Get2Elements(people, 1, 2));
        System.out.println("5)la Return two elements from the second" + lambda.task5Get2Elements(people, 1, 2));
        System.out.println("6)li Search '" + symbol + "': " + myList.task6SymbolSearch(people, symbol));
        System.out.println("6)la Search '" + symbol + "': " + lambda.task6SymbolSearch(people, symbol));
        System.out.println("7)li Rename objects: " + myList.task7RenameObject(people));
        System.out.println("7)la Rename objects: " + lambda.task7RenameObject(people));
        System.out.println("8)li Sorted by age: " + myList.task8SortAge(people));
        System.out.println("8)la Sorted by age: " + lambda.task8SortAge(people));
        System.out.println("8)li Sorted by name: " + myList.task8SortName(people));
        System.out.println("8)la Sorted by name: " + lambda.task8SortName(people));
        System.out.println("9)li Template for " + pattern + ": " + myList.task9TakePattern(people, pattern));
        System.out.println("9)la Template for " + pattern + ": " + lambda.task9TakePattern(people, pattern));
    }


}
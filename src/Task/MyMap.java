package Task;

import resource.Person;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;


public class MyMap {

    public Map<String, Person> task1RemoveMax(Map<String, Person> personMap) {
        Map<String, Person> resultMap = new HashMap<>();

        Iterator<String> it1 = personMap.keySet().iterator();
        Integer max = personMap.get(it1.next()).getAge();
        for (Map.Entry<String, Person> obj : personMap.entrySet()) {
            if (obj.getValue().getAge() > max) {
                max = obj.getValue().getAge();
            }
        }
        for (Map.Entry<String, Person> obj : personMap.entrySet()) {
            if (obj.getValue().getAge() != max) {
                resultMap.put(obj.getKey(), obj.getValue());
            }
        }

        return resultMap;
    }

    public Map<String, Person> task1RemoveMin(Map<String, Person> personMap) {
        Map<String, Person> resultMap = new HashMap<>();

        Iterator<String> it1 = personMap.keySet().iterator();
        Integer min = personMap.get(it1.next()).getAge();
        for (Map.Entry<String, Person> obj : personMap.entrySet()) {
            if (obj.getValue().getAge() < min) {
                min = obj.getValue().getAge();
            }
        }
        for (Map.Entry<String, Person> obj : personMap.entrySet()) {
            if (obj.getValue().getAge() != min) {
                resultMap.put(obj.getKey(), obj.getValue());
            }
        }

        return resultMap;
    }

    public Map<String, Person> task2RemoveMoreX(Map<String, Person> personMap, int x) {
        Map<String, Person> resultMap = new HashMap<>();
        for (Map.Entry<String, Person> obj : personMap.entrySet()) {
            if (obj.getValue().getAge() < x) {
                resultMap.put(obj.getValue().getName(), obj.getValue());
            }
        }

        return resultMap;
    }

    public Map<String, Person> task2RemoveLessX(Map<String, Person> personMap, int x) {
        Map<String, Person> resultMap = new HashMap<>();
        for (Map.Entry<String, Person> obj : personMap.entrySet()) {
            if (obj.getValue().getAge() > x) {
                resultMap.put(obj.getValue().getName(), obj.getValue());
            }
        }
        return resultMap;
    }

    public Integer task3TotalSum(Map<String, Person> personMap) {
        Integer sum = 0;
        for (Map.Entry<String, Person> obj : personMap.entrySet()) {
            sum += obj.getValue().getAge();
        }

        personMap.values().stream().filter((x) -> x.getAge() > 30).collect((Collectors.toMap(Person::getAge, Function.identity())));

        System.out.println("ALO ASD " + personMap.entrySet().stream().filter((obj) -> obj.getValue().getAge() > 30).collect(Collectors.toMap(Person::getName, Function.identity())));

        return sum;
    }

    public Person task4GetForIndex(Map<String, Person> personMap, int index) {
        Iterator<String> it1 = personMap.keySet().iterator();
        for (int i = 0; i <= index && it1.hasNext(); i++) {
            it1.next();
        }
        return index >= personMap.size() ? null : personMap.get(it1.next());
    }

    public Map<String, Person> task5Get2Elements(Map<String, Person> personMap, int start, int number) {
        Iterator<String> it1 = personMap.keySet().iterator();
        for (int i = 0; i <= start && it1.hasNext(); i++) {
            it1.next();
        }
        Map<String, Person> resultMap = new HashMap<>();
        for (int i = 0; i < number; i++) {
            String obj = it1.next();
            resultMap.put(obj, personMap.get(obj));
        }
        return resultMap;
    }

    public boolean task6SymbolSearch(Map<String, Person> personMap, CharSequence symbol) {
        for (Map.Entry<String, Person> obj : personMap.entrySet()) {
            if (!obj.getValue().getName().contains(symbol)) {
                return false;
            }
        }
        return true;
    }

    public Map<String, Person> task7RenameObject(Map<String, Person> personMap) {
        Map resultMap = new HashMap();
        for (Map.Entry<String, Person> obj : personMap.entrySet()) {
            resultMap.put(obj.getKey(), obj.getValue().toString() + "_1");
        }

        return resultMap;
    }

    public Map<String, Person> task8SortAge(Map<String, Person> personMap) {
        List<Map.Entry<String, Person>> list = new ArrayList(personMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Person>>() {
            @Override
            public int compare(Map.Entry<String, Person> o1, Map.Entry<String, Person> o2) {
                return (o1.getValue().getAge().compareTo(o2.getValue().getAge()));
            }
        });

        Map<String, Person> resultMap = new LinkedHashMap();
        for (Map.Entry<String, Person> entry : list) {
            resultMap.put(entry.getKey(), entry.getValue());
        }

        return resultMap;
    }

    public Map<String, Person> task8SortName(Map<String, Person> personMap) {
        List<Map.Entry<String, Person>> list = new ArrayList(personMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Person>>() {
            @Override
            public int compare(Map.Entry<String, Person> o1, Map.Entry<String, Person> o2) {
                return (o1.getValue().getName().compareTo(o2.getValue().getName()));
            }
        });

        Map<String, Person> resultMap = new LinkedHashMap();
        for (Map.Entry<String, Person> entry : list) {
            resultMap.put(entry.getKey(), entry.getValue());
        }

        return resultMap;
    }

    public Map<String, Person> task9TakePattern(Map<String, Person> personMap, String pattern) {
        Map<String, Person> resultMap = new HashMap<>();
        for (Map.Entry<String, Person> obj : personMap.entrySet()) {
            if (obj.getValue().getName().contains(pattern)) {
                resultMap.put(obj.getKey(), obj.getValue());
            }
        }

        return resultMap;
    }

}

package Task;

import resource.Person;

import java.util.*;
import java.util.stream.Collectors;

public class Lambda {

    public List<Person> task1RemoveMax(List<Person> personList) {
        return personList.stream().filter(elem -> elem.getAge() != personList.stream()
                .max(Comparator.comparing(Person::getAge)).get().getAge())
                .collect(Collectors.toList());
    }

    public List<Person> task1RemoveMin(List<Person> personList) {
        return personList.stream().filter(elem -> elem.getAge() != personList.stream()
                .min(Comparator.comparing(Person::getAge)).get().getAge())
                .collect(Collectors.toList());
    }

    public List<Person> task2RemoveMoreX(List<Person> personList, int x) {
        return personList.stream().filter((p) -> p.getAge() < x).collect(Collectors.toList());
    }

    public List<Person> task2RemoveLessX(List<Person> personList, int x) {
        return personList.stream().filter((p) -> p.getAge() > x).collect(Collectors.toList());
    }

    public Integer task3TotalSum(List<Person> personList) {
        return personList.stream().map(Person::getAge).reduce((s1, s2) -> s1 + s2).get();
    }

    public Person task4GetForIndex(List<Person> personList, int index) {
        return index >= personList.size() ? null : personList.stream().skip(index).findFirst().get();
    }

    public List<Person> task5Get2Elements(List<Person> personList, int start, int number) {
        return (start + number) >= personList.size() ? null : personList.stream().skip(start)
                .limit(number).collect(Collectors.toList());
    }

    public boolean task6SymbolSearch(List<Person> personList, CharSequence symbol) {
        return personList.stream().allMatch((x) -> x.getName().contains(symbol.toString()));
    }

    public List<Person> task7RenameObject(List<Person> personList) {
        List resultList = personList.stream().map((s) -> s + "_1").collect(Collectors.toList());
        return resultList;
    }

    public List<Person> task8SortAge(List<Person> personList) {
        return personList.stream().sorted(Comparator.comparing(Person::getAge)).collect(Collectors.toList());
    }

    public List<Person> task8SortName(List<Person> personList) {
        return personList.stream().sorted(Comparator.comparing(Person::getName)).collect(Collectors.toList());
    }

    public List<Person> task9TakePattern(List<Person> personList, String pattern) {
        return personList.stream().filter((s) -> s.getName().contains(pattern)).collect(Collectors.toList());
    }


}
